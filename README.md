In a Maze, you have a random mouse (M) and a cheese (C). You have to find the cheese by guiding the mouse. 
In the Maze Class, you have a method TryToMove(Direction) which takes a direction (U, D, L, R) that stands for (Up, Down, Left and Right) as parameter and try to move the mouse to that direction. If the move is possible, the mouse will move and the method will return true, else it will return false.
You also have a method FoundCheese that returns true if the mouse found the cheese and false if not.

You have a maze (on data/maze1.js), and you can change it to try another types of mazes. You goal is to return "Success !" in the console if the mouse found the cheese.