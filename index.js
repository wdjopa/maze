import Maze from "./model/Maze.js";
import { map } from "./data/maze1.js";

const Direction = {
  Up: "U",
  Down: "D",
  Left: "L",
  Right: "R",
};

(() => {
  let maze = new Maze(map);

  // Example of how to use the maze
  if (maze.TryMoveMouse(Direction.Up)) {
    if (maze.FoundCheese()) {
      console.log("Success!");
    } else {
      /// ...
    }
  } 
  /// 
})();
