class Maze {
  maze = [];
  cheese_position = [];
  mouse_position = [];
  
  constructor(maze) {
    this.maze = maze.map((line) => line.split(""));
    this.maze.forEach((line, i) => {
      line.forEach((char, j) => {
        if (char === "C") {
          this.cheese_position = [i, j];
        }
      });
    });
    this.maze.forEach((line, i) => {
      line.forEach((char, j) => {
        if (char === "M") {
          this.mouse_position = [i, j];
        }
      });
    });
  }

  /**
   * Returns true if the mouse is on the cheese.
   * @returns 
   */
  FoundCheese() {
    return this.cheese_position === this.mouse_position;
  }

  /**
   * Try to move the mouse in the given direction. If the move is possible,
   * the mouse is moved and the function returns true. If the move is not
   * possible, the mouse is not moved and the function returns false.
   * @param {String} direction : "U", "D", "L", "R"  
   * @returns 
   */
  TryMoveMouse(direction) {
    const [i, j] = this.mouse_position;
    switch (direction) {
      case "U":
        if (this.maze[i - 1] && this.maze[i - 1][j] && this.maze[i - 1][j] !== "X") {
          this.mouse_position = [i - 1, j];
          this.maze[i][j] = " ";
          this.maze[i - 1][j] = "M";
          return true;
        }
        return false;
      case "D":
        if (this.maze[i + 1] && this.maze[i + 1][j] && this.maze[i + 1][j] !== "X") {
          this.mouse_position = [i + 1, j];
          this.maze[i][j] = " ";
          this.maze[i + 1][j] = "M";
          return true;
        }
        return false;
      case "L":
        if (this.maze[i][j - 1] && this.maze[i][j - 1] !== "X") {
          this.mouse_position = [i, j - 1];
          this.maze[i][j] = " ";
          this.maze[i][j - 1] = "M";

          return true;
        }
        return false;
      case "R":
        if (this.maze[i][j + 1] && this.maze[i][j + 1] !== "X") {
          this.mouse_position = [i, j + 1];
          this.maze[i][j] = " ";
          this.maze[i][j + 1] = "M";
          return true;
        }
        return false;
    }
  }
}
export default Maze;
