// Ca ne marche meme pas

export function findCheeze(maze, prevDirection, sumAngles = 0, wishDirection, sens = 1) {
  // sens = 1 : sens horaire
  let dir1 = Direction.Up;
  let dir2 = Direction.Left;
  let dir3 = Direction.Down;
  let dir4 = Direction.Right;
  if (sens !== 1) {
    dir1 = Direction.Up;
    dir2 = Direction.Right;
    dir3 = Direction.Down;
    dir4 = Direction.Left;
  }

  if (wishDirection) {
    prevDirection = wishDirection;
    // if (maze.TryMoveMouse(wishDirection)) {
    //   if (maze.FoundCheese()) {
    //     console.log("Success!");
    //   } else {
    //     console.log(findCheeze(maze, wishDirection, sumAngles + 90, null));
    //   }
    // } else {
    //   console.log("No way");
    // }
  }
  if (prevDirection == dir3) {
    if (maze.TryMoveMouse(dir3)) {
      if (maze.FoundCheese()) {
        return "Success!";
      } else {
        return findCheeze(maze, dir3, sumAngles);
      }
    } else {
      if (sumAngles >= 360) {
        if (maze.TryMoveMouse(dir4)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            return findCheeze(maze, dir4, 90);
          }
        } else {
          if (maze.TryMoveMouse(dir1)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir1, sumAngles, dir1);
            }
          } else {
            return "No way";
          }
        }
      } else {
        if (maze.TryMoveMouse(dir2)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            if (wishDirection) {
              return findCheeze(maze, dir2, sumAngles, wishDirection);
            } else {
              return findCheeze(maze, dir2, sumAngles + 90);
            }
          }
        } else {
          if (maze.TryMoveMouse(dir1)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir1, sumAngles, dir2);
            }
          } else {
            return "No way";
          }
        }
      }
    }
  } else if (prevDirection == dir1) {
    if (maze.TryMoveMouse(dir1)) {
      if (maze.FoundCheese()) {
        return "Success!";
      } else {
        return findCheeze(maze, dir1, sumAngles);
      }
    } else {
      if (sumAngles >= 360) {
        if (maze.TryMoveMouse(dir2)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            return findCheeze(maze, dir2, 90);
          }
        } else {
          if (maze.TryMoveMouse(dir3)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir3, sumAngles, dir2);
            }
          } else {
            return "No way";
          }
        }
      } else {
        if (maze.TryMoveMouse(dir4)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            if (wishDirection) {
              return findCheeze(maze, dir4, sumAngles, wishDirection);
            } else {
              return findCheeze(maze, dir4, sumAngles + 90);
            }
          }
        } else {
          if (maze.TryMoveMouse(dir3)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir3, sumAngles, dir4);
            }
          } else {
            return "No way";
          }
        }
      }
    }
  } else if (prevDirection == dir2) {
    if (maze.TryMoveMouse(dir2)) {
      if (maze.FoundCheese()) {
        return "Success!";
      } else {
        return findCheeze(maze, dir2, sumAngles);
      }
    } else {
      if (sumAngles >= 360) {
        if (maze.TryMoveMouse(dir3)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            return findCheeze(maze, dir3, 90);
          }
        } else {
          if (maze.TryMoveMouse(dir4)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir4, sumAngles, dir3);
            }
          } else {
            return "No way";
          }
        }
      } else {
        if (maze.TryMoveMouse(dir1)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            if (wishDirection) {
              return findCheeze(maze, dir1, sumAngles, wishDirection);
            } else {
              return findCheeze(maze, dir1, sumAngles + 90);
            }
          }
        } else {
          if (maze.TryMoveMouse(dir4)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir4, sumAngles, dir1);
            }
          } else {
            return "No way";
          }
        }
      }
    }
  } else {
    if (maze.TryMoveMouse(dir4)) {
      if (maze.FoundCheese()) {
        return "Success!";
      } else {
        return findCheeze(maze, dir4, sumAngles);
      }
    } else {
      if (sumAngles >= 360) {
        if (maze.TryMoveMouse(dir1)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            return findCheeze(maze, dir1, 90);
          }
        } else {
          if (maze.TryMoveMouse(dir2)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir2, sumAngles, dir1);
            }
          } else {
            return "No way";
          }
        }
      } else {
        if (maze.TryMoveMouse(dir3)) {
          if (maze.FoundCheese()) {
            return "Success!";
          } else {
            if (wishDirection) {
              return findCheeze(maze, dir3, sumAngles, wishDirection);
            } else {
              return findCheeze(maze, dir3, sumAngles + 90);
            }
          }
        } else {
          if (maze.TryMoveMouse(dir2)) {
            if (maze.FoundCheese()) {
              return "Success!";
            } else {
              return findCheeze(maze, dir2, sumAngles, dir3);
            }
          } else {
            return "No way";
          }
        }
      }
    }
  }
}
